#include <iostream>

using namespace std;
const int tamanho=10;
int vetor[tamanho];

void inicializarVetor (int vet[], int tamanho){
    for (int i=0; i<tamanho;i++){
            (vet[i]=0;
    }
}

void imprimirVetor (int vet[],int tamanho){
    for (int i=0;i<tamanho;i++){
            cout<<"Posicao"<<(i+1)<<":"<<(vet[i])<<endl;
    }
}

void lerVetor (int vet[], int tamanho){
    for (int i=0;i<tamanho;i++){
            cout<<"Digite a"<<(i+1)<<"posicao:";
            cin>>(vet[i]);
    }
}

void bubbleSort (int vet[], int tamanho){
    for (int i=tamanho;i>=0;i--){
            for (int j=0;j<1;j++){
                    if(vet[j]>vet[j+1]){
                            int temp = vet[j];
                            vet [j] = vet[j+1];
                            vet[j+1] = temp;
                    }
            }
    }
}

int main(){
    inicializarVetor(vetor,tamanho);
    lerVetor(vetor,tamanho);
    bubbleSort(vetor,tamanho);
    imprimirVetor(vetor,tamanho);

return 0;
}
